# Descarga CFDI del SAT

## El uso de este proyecto tiene un costo de $100.00 anuales

## Script para descargar de forma automática documentos electrónicos del SAT.

### Mira el [wiki](https://gitlab.com/mauriciobaeza/cfdi-descarga/wikis/home) para los detalles de uso.

### Ayudanos a ayudar a otros, conoce [nuestras actividades](http://universolibre.org/hacemos/).

